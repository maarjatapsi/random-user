const app = Vue.createApp({
    data() {
        return {
            firstName: 'John',
            lastName: 'Doe',
            email: 'johndoe@gmail.com',
            gender: 'male',
            country: 'United States',
            city: 'Washington',
            cell: 45233556,
            picture: 'https://randomuser.me/api/portraits/men/75.jpg',
        }
    },
    methods: {
        async getUser() {
            const res = await fetch("https://randomuser.me/api");
            const { results } = await res.json()

            this.firstName = results[0].name.first
            this.lastName = results[0].name.last
            this.email = results[0].email
            this.gender = results[0].gender
            this.country = results[0].location.country
            this.city = results[0].location.city
            this.cell = results[0].cell
            this.picture = results[0].picture.large
        },
    },
})

app.mount('#app')